const _ = require('lodash')
const { assert } = require('chai')

const loca$h = require('../loca$h')

// CHUNK
describe('loca$h.chunk()', () => {
  const testArr = [ 1, 2, 3, 4, 5 ]
  it('should return 5 chunk', () => {
    assert.deepEqual(loca$h.chunk(testArr), _.chunk(testArr));
  });
  it('should return 3 chunk', () => {
    assert.deepEqual(loca$h.chunk(testArr, 2), _.chunk(testArr, 2));
  });
  it('should return 2 chunk', () => {
    assert.deepEqual(loca$h.chunk(testArr, 3), _.chunk(testArr, 3));
  });
  it('should return 1 chunk', () => {
    assert.deepEqual(loca$h.chunk(testArr, 5), _.chunk(testArr, 5));
  });
});

// CONCAT
describe('loca$h.concat()', () => {
  const testArr = [ 1, 2, 3, 4, 5 ]
  const arr1 = [ 6, 7, 8, 9 ]
  const arr2 = [ 10 ]
  const arr3 = []

  it('should return a ten element array', () => {
    assert.deepEqual(loca$h.concat(testArr, arr1, arr2, arr3), _.concat(testArr, arr1, arr2, arr3));
  });
})

// FIND INDEX
describe('loca$h.findIndex()', () => {
  const testArr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
  const elementToFind = 6
  const find = (element) => element === elementToFind

  it('should return 5', () => {
    assert.deepEqual(loca$h.findIndex(testArr, find, 3), _.findIndex(testArr, find, 3));
  });

  it('should return 5', () => {
    assert.deepEqual(loca$h.findIndex(testArr, find, 3), _.findIndex(testArr, find));
  });

  it('should return -1', () => {
    assert.deepEqual(loca$h.findIndex(testArr, find, 3), _.findIndex(testArr, find, 7));
  });
})

// FLATTEN DEEP
describe('loca$h.flattenDeep()', () => {
  const testArr = [ [ 1, [ 2, [ 3 ] ] ], 4, [ [ 5, 6 ], 7, [ 8, 9 ], 10 ] ]

  it('should return a flatten array', () => {
    assert.deepEqual(loca$h.flattenDeep(testArr), _.flattenDeep(testArr));
  });
})

// HEAD
describe('loca$h.head()', () => {
  const testArr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
  const testArr1 = []

  it('should return the head of the array', () => {
    assert.deepEqual(loca$h.head(testArr), _.head(testArr));
  });

  it('should return undefined', () => {
    assert.deepEqual(loca$h.head(testArr1), _.head(testArr1));
  });
})

// TAIL
describe('loca$h.tail()', () => {
  const testArr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
  const testArr1 = []

  it('should return the tail of the array', () => {
    assert.deepEqual(loca$h.tail(testArr), _.tail(testArr));
  });

  it('should return []', () => {
    assert.deepEqual(loca$h.tail(testArr1), _.tail(testArr1));
  });
})

// INTERSECTION
describe('loca$h.intersection()', () => {
  const testArr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
  const testArr1 = [ 1, 2, 5, 0, 22, 45 ]
  const testArr2 = []

  it('should return 3 elements', () => {
    assert.deepEqual(
      loca$h.intersection(testArr, testArr1),
      _.intersection(testArr, testArr1)
    );
  });

  it('should return empty set', () => {
    assert.deepEqual(
      loca$h.intersection(testArr, testArr1, testArr2),
      _.intersection(testArr, testArr1, testArr2)
    );
  });
})

// JOIN
describe('loca$h.join()', () => {
  const testArr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
  const separator = '$'

  it('should return ca$h separated string', () => {
    assert.deepEqual(loca$h.join(testArr, separator), _.join(testArr, separator));
  });
})

// map
describe('loca$h.map()', () => {
  const testArr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]

  const mapFunction = (element) => element * 2
  const mapFunction1 = (element) => element + 10

  it('should return elements doubled', () => {
    assert.deepEqual(loca$h.map(testArr, mapFunction), _.map(testArr, mapFunction));
  });

  if('should return elements plus 10', () => {
    assert.deepEqual(loca$h.map(testArr, mapFunction1), _.map(testArr, mapFunction1));
  });
})

// REDUCE
describe('loca$h.reduce()', () => {
  const testArr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]

  const reduceFunction = (accumulator, element) => accumulator + element
  const reduceFunction1 = (accumulator, element, index) => index === 0 ? accumulator : `${ accumulator }, ${ element }`


  it('should return sum of all elements', () => {
    assert.deepEqual(loca$h.reduce(testArr, reduceFunction), _.reduce(testArr, reduceFunction));
  });

  if('should return comma separated words', () => {
    assert.deepEqual(loca$h.reduce(testArr, reduceFunction1), _.reduce(testArr, reduceFunction1));
  });
})

// UNION
describe('loca$h.union()', () => {
  const testArr = [ 1, 2, 3, 4, 5 ]
  const testArr1 = [ 1, 2, 3, 4, 5, 6 ]
  const testArr2 = [ 4, 5, 7, 8, 9, 10 ]

  it('should return union of all elements without repetitions', () => {
    assert.deepEqual(loca$h.union(testArr, testArr1, testArr2), _.union(testArr, testArr1, testArr2));
  });
})

// FLAT MAP
describe('loca$h.flatMap()', () => {
  const testArr = [ [ 1 ], [ 2 ], [ 3 ], [ 4, 5 ] ]
  const mapFunction = (element) => element + 10

  it('should return flatten and mapped array', () => {
    assert.deepEqual(loca$h.flatMap(testArr, mapFunction), _.flatMap(testArr, mapFunction));
  });
})

// GROUPBY
describe('loca$h.groupBy()', () => {
  const testArr = [{
    key: 'a',
    value: '1'
  }, {
    key: 'b',
    value: '2'
  }, {
    key: 'c',
    value: '3'
  }, {
    key: 'd',
    value: '4'
  }]

  const groupFunction = ({ key }) => key

  it('should return collection', () => {
    assert.deepEqual(loca$h.groupBy(testArr, groupFunction), _.groupBy(testArr, groupFunction));
  });
})

