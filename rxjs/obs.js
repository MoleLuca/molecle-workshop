class PubSub {

  constructor() {
    this.queue = []
    this.observers = []
    this.operators = []
    this.resolved = false
  }

  next(value) {
    this.queue.push(value)

    const pipedValue = this.operators.reduce((acc, item) => acc.then(item), Promise.resolve(value))
    this.observers.forEach(observer => {
      pipedValue.then(observer)
    })
  }

  subscribe(observer) {
    this.observers.push(observer)
  }

  pipe(...operators) {
    this.operators = [...this.operators, ...operators ]
  }

  of(...params) {
    params.forEach((param) => {
      if (Array.isArray(param)) {
        const p = [ ...param ]
        p.forEach(this.next.bind(this))
      } else {
        this.next(param)
      }
    })
    this.resolved = true;
  }

}

module.exports = new PubSub()
