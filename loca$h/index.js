/**
 * Creates an array of elements split into groups the length of size. If array can't be split evenly, the final chunk will be the remaining elements.

 *
 * @param {Array} array
 * @param {Number} size
 *
 * @returns {Array}
 */
module.exports.chunk = (array, size=1) => {
  return array
}

/**
 * Creates a new array concatenating array with any additional arrays and/or values.
 *
 * @param {Array} array
 * @param {...any} values
 *
 * @returns {Array}
 */
module.exports.concat = (array, ...values) => {
  return array
}


/**
 * This method is like _.find except that it returns the index of the first element predicate returns truthy for instead of the element itself.
 *
 * @param {Array} array
 * @param {Function} predicate
 * @param {Number} fromIndex
 *
 * @returns {Number}
 */
module.exports.findIndex = (array, predicate, fromIndex=0) => {
  return -1
}

/**
 * Recursively flatten array up to depth times.
 *
 * @param {Array} array
 *
 * @returns {Array}
 */
module.exports.flattenDeep = (array) => {
  return array
}

/**
 * Gets the first element of array.
 *
 * @param {Array} array
 *
 * @returns {any}
 */
module.exports.head = (array) => {
  return ({})
}

/**
 * Gets all but the first element of array.
 *
 * @param {Array} array
 *
 * @returns {Array}
 */
module.exports.tail = (array) => {
  return array
}

/**
 * Creates an array of unique values that are included in all given arrays using SameValueZero for equality comparisons. The order and references of result values are determined by the first array.
 *
 * @param {...Array} arrays
 *
 * @returns {Array}
 */
module.exports.intersection = (...arrays) => {
  return array
}

/**
 * Converts all elements in array into a string separated by separator.
 * 
 * @param {Array} array
 * @param {String} separator
 *
 * @returns {String}
 */
module.exports.join = (array, separator) => {
  return ''
}

/**
 * Creates an array of values by running each element in array thru iteratee. The iteratee is invoked with three arguments:
(value, index|key, array).
 *
 * @param {Array} array
 * @param {Function} iteratee
 *
 * @returns {Array}
 */
module.exports.map = (array, iteratee) => {
  return []
}

/**
 * Reduces array to a value which is the accumulated result of running each element in array thru iteratee, where each successive invocation is supplied the return value of the previous. If accumulator is not given, the first element of array is used as the initial value. The iteratee is invoked with four arguments:
(accumulator, value, index|key, array).
 *
 * @param {Array} array
 * @param {Function} iteratee
 *
 * @returns {Array}
 */
module.exports.reduce = (array, iteratee) => {
  return
}

/**
 * Creates an array of unique values, in order, from all given arrays using SameValueZero for equality comparisons.
 *
 * @param {Array} arrays
 *
 * @returns {Array}
 */
module.exports.union = (...array) => {
  return array
}

/**
 * Creates a flattened array of values by running each element in array thru iteratee and flattening the mapped results. The iteratee is invoked with three arguments: (value, index|key, array).
 *
 * @param {Array} array
 * @param {Function} iteratee
 *
 * @returns {Array}
 */
module.exports.flatMap = (array, iteratee) => {
  return []
}

/**
 * Creates an object composed of keys generated from the results of running each element of collection thru iteratee. The order of grouped values is determined by the order they occur in collection. The corresponding value of each key is an array of elements responsible for generating the key. The iteratee is invoked with one argument: (value).
 *
 * @param {Array|Collection} collection
 * @param {Function} iteratee
 *
 * @returns {any}
 */
module.exports.groupBy = (collection, iteratee) => {
  return ({})
}
