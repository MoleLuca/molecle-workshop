const numbers = [
  2935286972,
  3466645416,
  1399468546,
  2754664889,
  2159867243,
  2174546724,
  1410359150,
  2529267312,
  3536517774,
  1033623919,
  3465733992,
  2342844023,
  1846107827,
]

const expectedNumbers = [
  'The value is: 2935286982',
  'The value is: 3466645426',
  'The value is: 1399468556',
  'The value is: 2754664899',
  'The value is: 2159867253',
  'The value is: 2174546734',
  'The value is: 1410359160',
  'The value is: 2529267322',
  'The value is: 3536517784',
  'The value is: 1033623929',
  'The value is: 3465734002',
  'The value is: 2342844033',
  'The value is: 1846107837',
  'The value is: 20',
] 

// describe("Array test", function() {
//   const Observable = require('../../rxjs/obs')
//   obs.pipe(
//     (value) => value + 10,
//     (value) => `The value is: ${ value }`,
//   )
//   it("Array as param", function() {
//     obs.subscribe((value) => console.log(value))
//     expect(numbers).toBe(true);
//   });
// });

const pubsub = () => {
  const PubSub = require('../../rxjs/obs')

  PubSub.pipe(
    (value) => value + 10,
    (value) => `The value is: ${ value }`,
  )
  
  PubSub.subscribe(async (value) => console.log(value))
  
  PubSub.of(numbers, [10])

}

pubsub()